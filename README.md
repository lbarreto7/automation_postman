# README #

## Configurar ambiente para automação dos testes da API

### Instalação e Configuração, para executar os testes da API pelo terminal (Command Line)

#### Abrir o terminal e executar os seguintes passos:

* Instalar homebrew

	$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
>

* Instalar Node.js

	$ brew install node
>

* Instalar Newman

	$ npm install -g newman
>

* Instalar Relatorio html

	$ npm install -g newman-reporter-html
>

* Clonar o repositório

	$ git clone https://bitbucket.org/neon_pagamentos_backend/neon-qa-postman-automation/src
>

#### Para rodar os testes no ambiente de DEV, execute o seguinte comando no terminal:
>
	$ newman run -e DEV.postman_environment.json -r cli,html --reporter-html-template templates/htmlreqres.hbs --reporter-html-export report_test_api.html Neon.postman_collection.json

>

## Instalação e Configuração, para executar os testes da API pelo Postman

### Baixar o Postman:

* Link app Postman

	$ https://www.getpostman.com/apps
>

* Clonar o repositório

	$ git clone https://bitbucket.org/neon_pagamentos_backend/neon-qa-postman-automation/src

### Importar a collection e environment baixada do repositório para o app do Postman


## Agora é só executar e ser feliz =)
